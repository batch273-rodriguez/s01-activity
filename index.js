/* Activity - Quiz

Quiz:
1. How do you create arrays in JS?
let arrExample = ["item1","item2"];

2. How do you access the first character of an array?
arrExample[0].charAt(0))

3. How do you access the last character of an array?
arrExample[arrExample.length-1].charAt(arrExample[arrExample.length-1].length-1)

4. What array method searches for, and returns the index of a given value in an array? This method returns -1 if given value is not found in the array.
IndexOf()

5. What array method loops over all elements of an array, performing a user-defined function on each iteration?
forEach()

6. What array method creates a new array with elements obtained from a user-defined function?
map()

7. What array method checks if all its elements satisfy a given condition?
every()

8. What array method checks if at least one of its elements satisfies a given condition?
some()

9. True or False: array.splice() modifies a copy of the array, leaving the original unchanged.
False

10.True or False: array.slice() copies elements from original array and returns them as a new array.
True


*/


// Activity - Function Coding 

let students = ["John", "Joe", "Jane", "Jessie"];
console.log(students);

// 1. addToEnd()

function addToEnd (array, value) {
    if (typeof(value) === "string"){
        array.push(value);
        return array;
    } else {
        return `error - can only add strings to an array`;
    }
}

// 2. addToStart()

function addToStart (array, value) {
    if (typeof(value) === "string"){
        array.unshift(value);
        return array;
    } else {
        return `error - can only add strings to an array`;
    }
}

// 3. elementChecker()

function elementChecker(array, value) {
    if(array.length < 1){
        return `error - passed in array is empty`;
    } else {
        // console.log('array not empty');
        return array.some( (item) => {
            return item === value;
        }       )
    }
}

// 4. checkAllStringsEnding()

function checkAllStringsEnding(arr, value) {
    if(arr.length < 1){
        return `error - array must NOT be empty`;
    } else {
        if( arr.some( (item) => {return typeof(item) !== "string";})){
            // console.log('one item is not string')
            return `error - all array elements must be strings`;
        }else{
            // console.log('all items are string');
            if(typeof(value)!=="string"){
                return `error - 2nd argument must be of data type string`;
            }else{
                // console.log(value.length);
                if(value.length>1){
                    return `error - 2nd argument must be a single character`;
                }else{
                    
                    if( arr.some( (item) => {
                        // console.log(item);
                        // console.log(value);
                        // console.log(item.charAt(item.length-1));
                        return item.charAt(item.length-1) !== value;
                    })){
                        return false;
                    } else {
                        return true;
                    }
                }
                
            }
            
        }

    }
    
}

// 5.stringLengthSorter()

function stringLengthSorter(arr) {

    if(arr.some((item)=>{
        // console.log(typeof(item)!=="string");
        return typeof(item)!=="string";
    })){
        return `error - all array elements must be strings`;
    } else {
    //    console.log("pass-all items are string");
        let arrSorted = [];
        arr.forEach((item) => {
            // console.log(arrSorted.length);
            if (arrSorted.length < 1) {
                arrSorted.push(item);
            }
            else {
                // console.log(`item: ${item}`);
                // console.log(`item length: ${item.length}`);
                // console.log(`array: ${arrSorted}`);
                // console.log(`array length: ${arrSorted[0].length}`);
                // console.log(item.length<=arrSorted[0].length);
                if(item.length<=arrSorted[0].length){
                    // console.log("unshift");
                    arrSorted.unshift(item);
                } else{
                    // console.log(false);
                    // console.log(`item: ${item}`);
                    // console.log(`item length: ${item.length}`);
                    // console.log(`array: ${arrSorted}`);
                    // console.log(`array length: ${arrSorted.length}`);
                    let i = 0;
                    let splice = true;
                    while (i<arrSorted.length && splice){
                        // console.log(arrSorted[i].length);
                        if(item.length<=arrSorted[i].length){
                            arrSorted.splice(i,0,item);
                            splice = false;
                        }else{
                            arrSorted.push(item);
                            splice = false;
                        }
                        i++
                        
                    }
                }
            }

            
        });
        students = arrSorted;
        return students;
    }

}


// 8. randomPicker()

function randomPicker(arr) {
    let rand = Math.floor(Math.random() * arr.length);
    console.log(rand);
    return arr[rand];
}